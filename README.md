gb devops
# Микросервисная архитектура и контейнеризация
Урок 4
```
$ sudo apt-get install postgresql-client  

$ k port-forward postgres-5bd47c5b75-hsgh4 5432:5432  
  
$ psql -h localhost -U postgresadmin --password postgresdba  
postgresdb=# create table testtable (name VARCHAR (50));
CREATE TABLE
postgresdb=# \dt
             List of relations
 Schema |   Name    | Type  |     Owner
--------+-----------+-------+---------------
 public | testtable | table | postgresadmin
(1 row)

postgresdb=# insert into testtable values ('one');
INSERT 0 1
postgresdb=# insert into testtable values ('two');
INSERT 0 1
postgresdb=# select * from testtable;
 testcolumn
------------
 one
 two
(2 rows)
```
