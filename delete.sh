kubectl delete deployment postgres
kubectl delete configmap postgres-config
kubectl delete secret postgres-secret
kubectl delete persistentvolumeclaim postgres-pv-claim
kubectl delete persistentvolume postgres-pv-volume
